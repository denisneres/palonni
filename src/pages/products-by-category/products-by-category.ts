import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import WC from 'woocommerce-api';
import { ProductDetailsPage } from '../product-details/product-details';

@Component({
  selector: 'page-products-by-category',
  templateUrl: 'products-by-category.html',
})
export class ProductsByCategoryPage {

  WooCommerce: any;
  products: any[];
  moreProducts: any[];
  page: number;
  category: any;


  constructor(public navCtrl: NavController, private _loadingCtrl: LoadingController, public navParams: NavParams) {

    let loading = this._loadingCtrl.create({
      content: 'Carregando...'
    });

    loading.present();

    this.WooCommerce = WC({
      url: "http://localhost/wordpress",
      consumerKey: 'ck_0c4c0395471f5bfc085d017047070e5b0e91b7e3',
      consumerSecret: 'cs_9aba85cfb29ef76dba3d0ca68c303199a55ae197'

    });

    this.page = 1;
    this.category = navParams.get('category');

    this.WooCommerce.getAsync('products?filter[category]=' + this.category.slug).then((data) => {
      this.products = JSON.parse(data.body).products;
      loading.dismiss();

    }, (err) => {
      console.log("erro");
      console.log(err);
    }
    );


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductsByCategoryPage');
  }

  loadMoreProducts(event) {

    this.page++;

    this.WooCommerce.getAsync("products?filter[category]=" + this.category.slug + "&page=" + this.page).then((data) => {

      this.moreProducts = this.moreProducts.concat(JSON.parse(data.body).products);

      if (event != null) {
        event.complete();
      }

    }, (err) => {
      console.log("erro");
      console.log(err);
    }
    );
  }
  goToDetails(product) {
    this.navCtrl.push(ProductDetailsPage, {product});
  }
  
}
