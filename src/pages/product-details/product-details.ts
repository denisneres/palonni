import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ModalController } from 'ionic-angular';
import WC from 'woocommerce-api';

import { Storage } from '@ionic/storage';
import { CartPage } from '../cart/cart';
@Component({
  selector: 'page-product-details',
  templateUrl: 'product-details.html',
})
export class ProductDetailsPage {

  product: any;
  WooCommerce: any;
  reviews: any[] = [];


  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public storage: Storage,
              private toastCtrl: ToastController,
              private modalCtrl: ModalController) {

    this.product = this.navParams.get('product');

    this.WooCommerce = WC({
      url: "http://localhost/wordpress",
      consumerKey: 'ck_0c4c0395471f5bfc085d017047070e5b0e91b7e3',
      consumerSecret: 'cs_9aba85cfb29ef76dba3d0ca68c303199a55ae197'

    });

    this.WooCommerce.getAsync('products/' + this.product.id + '/reviews').then((data) => {
      this.reviews = JSON.parse(data.body).product_reviews;
    }, (err) => {
      console.log(err);
    });
  }

  addToCart(product) {
    
    this.storage.get('cart').then((productsInCart) => {
      console.log(productsInCart);
      if (productsInCart == null || productsInCart.length == 0) {

        productsInCart = [];
        this.addNewProduct(productsInCart, product);

      } else {
        var index = productsInCart.findIndex(p => p.product.id == product.id);
        if (index != null && index != -1) {
          this.increaseAmountOnCart(productsInCart, product, index);
        } else {
          this.addNewProduct(productsInCart, product);
        }
      }

      this.storage.set("cart", productsInCart).then(()=> {
        this.toastCtrl.create({
          message:"Produto adicionado ao carrinho!",
          duration:3000
        }).present();
      })
  });
}

  addNewProduct(productsInCart, product) {
    productsInCart.push({
      "product": product,
      "qtd": 1,
      "amount": parseFloat(product.price)
    });
  }

  increaseAmountOnCart(productsInCart, product, index) { 
    console.log(index);
    console.log(productsInCart[index]);
    productsInCart[index].qtd++;
    productsInCart[index].amount = parseFloat(productsInCart[index].amount) + parseFloat(product.price);
  }

  openCart() {
    this.modalCtrl.create(CartPage).present();
  }
}
