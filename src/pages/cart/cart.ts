import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {

  cartItens: any[];
  total: number;
  showEmptyCartMessage = false;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public storage: Storage,
              public viewCtrl: ViewController) {
    this.total = 0.0;

    this.storage.ready().then(() => {
      this.storage.get('cart').then((data) => {
        this.cartItens = data;
        
        if(this.cartItens.length > 0) {
          this.cartItens.forEach((item, index) => {
            this.total += (item.product.price * item.qtd);

          })
        } else {
          this.showEmptyCartMessage = true;
        }
        
      });
    });
  }

  removerFromCart(item, i) {
    let price = item.price;
    let qtd = item.qtd;

    this.cartItens.splice(i, 1);

    this.storage.set("cart", this.cartItens).then(() => {
      this.total = this.total - (price * qtd);
    })
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

}
