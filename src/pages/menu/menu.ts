import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import  WC  from 'woocommerce-api';
import { ProductsByCategoryPage } from '../products-by-category/products-by-category';

@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  public homePage: any;e
  WooCommerce: any;
  categories: any[] = [{}];
  @ViewChild('content') childNavCtrl: NavController;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.homePage = HomePage;

    this.WooCommerce = WC({
      url: "http://localhost/wordpress",
      consumerKey: 'ck_0c4c0395471f5bfc085d017047070e5b0e91b7e3',
      consumerSecret: 'cs_9aba85cfb29ef76dba3d0ca68c303199a55ae197'
    });

    this.WooCommerce.getAsync("products/categories").then(data => {
        console.log(JSON.parse(data.body).product_categories);

        this.mountMenuCategories(JSON.parse(data.body).product_categories);

    }, err => {
      console.log("error");
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuPage');
  }
  
  mountMenuCategories(categories) {
    this.categories = categories;
    this.categories = this.categories.filter(cat => !(cat.slug == "roupas"));

    this.categories.forEach(cat => {
      if(cat.slug == "blusas") {
        cat.icon = "shirt";
        console.log(cat.icon);
      } else if (cat.slug == "calcados") {
        cat.icon = "heart";
      } else if (cat.slug == "saias") {
        cat.icon = "image";
      } else if (cat.slug == "vestidos") {
        cat.icon = "leaf";
      } else if (cat.slug == "calcas") {
        cat.icon = "grid";
      }
    })
  }

  goToProductsByCategory(category) {
    this.childNavCtrl.setRoot(ProductsByCategoryPage, {'category': category});
  }
}
