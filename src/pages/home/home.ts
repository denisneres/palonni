import { Component, ViewChild } from '@angular/core';
import { NavController, Slides, LoadingController } from 'ionic-angular';

import * as WC from 'woocommerce-api';
import { ProductDetailsPage } from '../product-details/product-details';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  products: any[] = [{}];
  moreProducts: any[] = [{}];
  page = 2;
  WooCommerce: any;

  @ViewChild('productSlides') productSlides: Slides;

  constructor(public navCtrl: NavController, public loadCtrl: LoadingController) {

    let loading = loadCtrl.create({
      content:"Carregando produtos..."
    });
    loading.present();

    this.WooCommerce = WC({
      url: "http://localhost/wordpress",
      consumerKey: 'ck_0c4c0395471f5bfc085d017047070e5b0e91b7e3',
      consumerSecret: 'cs_9aba85cfb29ef76dba3d0ca68c303199a55ae197'

    });

    
    this.WooCommerce.getAsync('products').then((data) => {
      
      this.products = JSON.parse(data.body).products;
      loading.dismiss();
    }, (err) => {
      console.log("erro");
      console.log(err);
    }
    );
    this.loadMoreProducts(null);
  }

  ionViewDidLoad() {
    setInterval(() => {
      if(this.productSlides.getActiveIndex() == this.productSlides.length()-1) {
        this.productSlides.slideTo(0);
      }
      this.productSlides.slideNext();
      }, 3000)
    }

  loadMoreProducts(event) {

      if(event == null) {
        this.page = 2;
        this.moreProducts = [];
      } else {
        this.page++;
      }

    this.WooCommerce.getAsync('products?page='+this.page).then((data) => {
      
      this.moreProducts = this.moreProducts.concat(JSON.parse(data.body).products);

      if(event != null) {
        event.complete();
      }

    }, (err) => {
      console.log("erro");
      console.log(err);
    }
    );
  }

  goToDetails(product) {
    this.navCtrl.push(ProductDetailsPage, {product});
  }
}
